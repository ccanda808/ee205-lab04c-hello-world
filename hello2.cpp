///////////////////////////////////////////////////////////////////////////////
////// University of Hawaii, College of Engineering
////// EE 205 - Object Oriented Programming
////// Lab 04c - hello2
//////
////// Usage:  hello2
//////
////// Result:
//////  prints "Hello World!" followed by a new line
//////
////// @author Cameron Canda <ccanda@hawaii.edu>
////// @date  14_February_2021
///////////////////////////////////////////////////////////////////////////////////

#include<iostream>

int main() {

   std::cout << "Hello World!" << std::endl;

   return 0;

}
