///////////////////////////////////////////////////////////////////////////////
//// University of Hawaii, College of Engineering
//// EE 205 - Object Oriented Programming
//// Lab 04c - hello1
////
//// Usage:  hello1
////
//// Result:
////  prints "Hello World!" followed by a new line
////
//// @author Cameron Canda <ccanda@hawaii.edu>
//// @date  14_February_2021
/////////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main() {

   cout << "Hello World!" << endl;

   return 0;

}
